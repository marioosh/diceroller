package net.fivedots.diceroller.filter;

import org.junit.Test;

import static junit.framework.Assert.assertTrue;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 26.05.2014
 * Time: 15:01
 */
public class FilterTest {
    @Test
    public void numericDiceFilter() throws Exception {

        NumericDiceFilter ndf = new NumericDiceFilter();
        assertTrue(ndf.match("3k6rr"));
    }

    @Test
    public void numericDiceFilter2() throws Exception {

        NumericDiceFilter ndf = new NumericDiceFilter();
        assertTrue(ndf.match("3k6c"));
    }

    @Test
    public void numericDiceFilter3() throws Exception {

        NumericDiceFilter ndf = new NumericDiceFilter();
        assertTrue(ndf.match("3d8c"));
    }

    @Test
    public void numericDiceFilter4() throws Exception {

        NumericDiceFilter ndf = new NumericDiceFilter();
        assertTrue(ndf.match("d6b"));
    }

    @Test
    public void numericDiceFilter5() throws Exception {

        NumericDiceFilter ndf = new NumericDiceFilter();
        assertTrue(ndf.match("k6rr"));
    }

    @Test
    public void customNumericFilter() {
        CustomNumericDiceFilter f = new CustomNumericDiceFilter();
        assertTrue(f.match("3custom[1;2;3;4;5;6]"));
    }


    @Test
    public void customNumericFilter2() {
        CustomNumericDiceFilter f = new CustomNumericDiceFilter();
        assertTrue(f.match("3custom[1;2:2;2:5;4]ih"));
    }

    @Test
    public void customNumericFilter3() {
        CustomNumericDiceFilter f = new CustomNumericDiceFilter();
        assertTrue(f.match("custom[1;2:2;2:5;4]il"));
    }

    @Test
    public void customNumericFilter3Short() {
        CustomNumericDiceFilter f = new CustomNumericDiceFilter();
        assertTrue(f.match("cu[1;2:2;2:5;4]il"));
    }

    @Test
    public void customTextualFilter() {
        CustomTextualDiceFilter f = new CustomTextualDiceFilter();
        assertTrue(f.match("3custom{Blank;Focus;Hit;Critical}c4"));
    }

    @Test
    public void customTextualFilterShort() {
        CustomTextualDiceFilter f = new CustomTextualDiceFilter();
        assertTrue(f.match("3cu{Blank;Focus;Hit;Critical}c4"));
    }

    @Test
    public void customTextualFilter2() {
        CustomTextualDiceFilter f = new CustomTextualDiceFilter();
        assertTrue(f.match("custom{Blank;Focus;Hit;Critical}b"));
    }

    @Test
    public void customTextualFilter3() {
        CustomTextualDiceFilter f = new CustomTextualDiceFilter();
        assertTrue(f.match("3custom{Blank;2:Focus;2:Hit;Critical}="));
    }

    @Test
    public void customTextualFilter4() {
        CustomTextualDiceFilter f = new CustomTextualDiceFilter();
        assertTrue(f.match("custom{Blank;2:Focus;2:Hit;Critical}s"));
    }
}
