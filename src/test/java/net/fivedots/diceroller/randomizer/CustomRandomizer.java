package net.fivedots.diceroller.randomizer;

import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Queue;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 15.05.2014
 * Time: 11:31
 * USE WITH K6 Dices!
 */
public class CustomRandomizer implements Randomizer {
    private Queue<Integer> expectations;

    public CustomRandomizer() {
        expectations = new LinkedList<>();
        final int[] exp = {2, 1, 6, 5, 3, 4, 6, 3, 1, 5, 2, 4};
        fillExpectations(exp);
    }

    public CustomRandomizer(int... expectations) {
        this.expectations = new LinkedList<>();
        fillExpectations(expectations);
    }

    @Override
    public int rollDice(int max) {

        int element;
        try {
            element = expectations.remove();
        } catch (NoSuchElementException nsee) {
            return 1;
        }
        //add at the end
        expectations.offer(element);
        return (element < max) ? element : max;
    }

    public void fillExpectations(int[] expectations) {
        for (int i : expectations) {
            this.expectations.add(i);
        }
    }
}
