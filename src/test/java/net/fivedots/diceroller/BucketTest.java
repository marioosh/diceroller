package net.fivedots.diceroller;

import net.fivedots.diceroller.randomizer.CustomRandomizer;
import org.junit.Test;

import java.util.List;
import java.util.Queue;

import static junit.framework.Assert.*;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 26.05.2014
 * Time: 14:54
 */
public class BucketTest {
// ------------------------------ FIELDS ------------------------------

    private Bucket bucket;

    private Result result;

// -------------------------- OTHER METHODS --------------------------

    @Test
    public void basicDice() throws DiceRollerException {
        Bucket bucket = new Bucket();
        bucket.parseQuery("3k6");
        assertNotNull(bucket.getDice());
        assertEquals(Integer.class, bucket.getDice().getClazz());
        assertEquals(6, bucket.getDice().getSides());
        assertEquals(3, bucket.getNumberOfDices());
    }

    @Test
    public void basicDice2() throws DiceRollerException {
        Bucket bucket = new Bucket();
        bucket.parseQuery("d6");
        assertNotNull(bucket.getDice());
        assertEquals(Integer.class, bucket.getDice().getClazz());
        assertEquals(6, bucket.getDice().getSides());
        assertEquals(1, bucket.getNumberOfDices());
        assertTrue(bucket.getDice().getElements().isEmpty());
    }

    @Test
    public void customDice() throws DiceRollerException {
        Bucket bucket = new Bucket();
        bucket.parseQuery("2custom[1;2;3;4;5;6;7;8]");
        assertNotNull(bucket.getDice());
        assertEquals(Integer.class, bucket.getDice().getClazz());
        assertEquals(8, bucket.getDice().getSides());
        assertEquals(2, bucket.getNumberOfDices());
        Queue sides = bucket.getDice().getElements();
        assertNotNull(sides);
        assertEquals(1, sides.peek());
        assertEquals(8, ((List) sides).get(sides.size() - 1));
    }

    @Test
    public void customDice2() throws DiceRollerException {
        Bucket bucket = new Bucket();
        bucket.parseQuery("12custom[2;3;4;5;6;7;8]");
        assertNotNull(bucket.getDice());
        assertEquals(Integer.class, bucket.getDice().getClazz());
        assertEquals(7, bucket.getDice().getSides());
        assertEquals(12, bucket.getNumberOfDices());
        Queue sides = bucket.getDice().getElements();
        assertNotNull(sides);
        assertEquals(2, sides.peek());
        assertEquals(8, ((List) sides).get(sides.size() - 1));
    }

    @Test
    public void customDice2Shortcut() throws DiceRollerException {
        Bucket bucket = new Bucket();
        bucket.parseQuery("12cu[2;3;4;5;6;7;8]");
        assertNotNull(bucket.getDice());
        assertEquals(Integer.class, bucket.getDice().getClazz());
        assertEquals(7, bucket.getDice().getSides());
        assertEquals(12, bucket.getNumberOfDices());
        Queue sides = bucket.getDice().getElements();
        assertNotNull(sides);
        assertEquals(2, sides.peek());
        assertEquals(8, ((List) sides).get(sides.size() - 1));
    }

    @Test
    public void customTextualDice() throws DiceRollerException {
        Bucket bucket = new Bucket();
        bucket.parseQuery("4custom{Blank;Focus;Hit;Critical}");
        assertNotNull(bucket.getDice());
        assertEquals(String.class, bucket.getDice().getClazz());
        assertEquals(4, bucket.getDice().getSides());
        assertEquals(4, bucket.getNumberOfDices());
        Queue sides = bucket.getDice().getElements();
        assertNotNull(sides);
        assertEquals("Blank", sides.peek());
        assertEquals("Critical", ((List) sides).get(sides.size() - 1));
    }

    @Test
    public void customTextualDice2() throws DiceRollerException {
        Bucket bucket = new Bucket();
        bucket.parseQuery("14custom{Blank;2:Focus;3:Hit;Critical}");
        assertNotNull(bucket.getDice());
        assertEquals(String.class, bucket.getDice().getClazz());
        assertEquals(7, bucket.getDice().getSides());
        assertEquals(14, bucket.getNumberOfDices());
        Queue sides = bucket.getDice().getElements();
        assertNotNull(sides);
        assertEquals("Blank", sides.peek());
        assertEquals("Critical", ((List) sides).get(sides.size() - 1));
    }

    @Test
    public void customTextualDice2Shortcut() throws DiceRollerException {
        Bucket bucket = new Bucket();
        bucket.parseQuery("14cu{Blank;2:Focus;3:Hit;Critical}");
        assertNotNull(bucket.getDice());
        assertEquals(String.class, bucket.getDice().getClazz());
        assertEquals(7, bucket.getDice().getSides());
        assertEquals(14, bucket.getNumberOfDices());
        Queue sides = bucket.getDice().getElements();
        assertNotNull(sides);
        assertEquals("Blank", sides.peek());
        assertEquals("Critical", ((List) sides).get(sides.size() - 1));
    }

    @Test
    public void explodeInversed() {
        makeBucket("3k6ir");
        assertResult(2, 1, 6, 5);
    }


    @Test
    public void explodeInversedWithBoundaries() {
        makeBucket("3k6ir2");
        assertResult(2, 1, 6, 5, 3);
    }

    @Test
    public void explodeRecursive() {
        makeBucket("6d6rr");
        assertResult(2, 1, 6, 5, 3, 4, 6, 3);
    }

    @Test
    public void explodeRecursiveInversedWithBoundaries() {
        makeBucket("3d6irr3");
        assertResult(2, 1, 6, 5, 3, 4);
    }

    @Test
    public void explodeRecursiveWithBoundaries() {
        makeBucket("6d6rr5");
        assertResult(2, 1, 6, 5, 3, 4, 6, 3, 1);
    }

    @Test
    public void filters() throws DiceRollerException {
        Bucket bucket = new Bucket();
        bucket.parseQuery("3k6");
        assertNotNull(bucket);
    }

    @Test
    public void getElementFromList() throws DiceRollerException {
        Bucket bucket = new Bucket();
        bucket.parseQuery("14custom{Blank;2:Focus;3:Hit;Critical}");
        assertNotNull(bucket.getDice());
        assertEquals(String.class, bucket.getDice().getClazz());
        Dice dice = bucket.getDice();

        assertEquals("Blank", dice.getElementAtPosition(1));
        assertEquals("Focus", dice.getElementAtPosition(2));
        assertEquals("Focus", dice.getElementAtPosition(3));
        assertEquals("Hit", dice.getElementAtPosition(4));
        assertEquals("Hit", dice.getElementAtPosition(5));
        assertEquals("Hit", dice.getElementAtPosition(6));
        assertEquals("Critical", dice.getElementAtPosition(7));
    }

    @Test
    public void getElementFromListNumerical() throws DiceRollerException {
        Bucket bucket = new Bucket();
        bucket.parseQuery("12custom[2;3;4;5;6;7;8]");
        assertNotNull(bucket.getDice());
        assertEquals(Integer.class, bucket.getDice().getClazz());
        Dice dice = bucket.getDice();

        assertEquals(2, dice.getElementAtPosition(1));
        assertEquals(3, dice.getElementAtPosition(2));
        assertEquals(4, dice.getElementAtPosition(3));
        assertEquals(5, dice.getElementAtPosition(4));
        assertEquals(6, dice.getElementAtPosition(5));
        assertEquals(7, dice.getElementAtPosition(6));
        assertEquals(8, dice.getElementAtPosition(7));
    }

    @Test
    public void getIndexFor() throws DiceRollerException {
        Bucket bucket = new Bucket();
        bucket.parseQuery("12custom[2;3;4;4;6;6;8]");
        assertNotNull(bucket.getDice());
        assertEquals(Integer.class, bucket.getDice().getClazz());
        Dice dice = bucket.getDice();

        assertEquals(0, dice.getIndexFor(2));
        assertEquals(1, dice.getIndexFor(3));
        assertEquals(2, dice.getIndexFor(4));
        assertEquals(4, dice.getIndexFor(6));
        assertEquals(6, dice.getIndexFor(8));
    }

    @Test
    public void getIndexForString() throws DiceRollerException {
        Bucket bucket = new Bucket();
        bucket.parseQuery("14custom{Blank;2:Focus;3:Hit;Critical}");
        assertNotNull(bucket.getDice());
        assertEquals(String.class, bucket.getDice().getClazz());
        Dice dice = bucket.getDice();

        assertEquals(0, dice.getIndexFor("Blank"));
        assertEquals(1, dice.getIndexFor("Focus"));
        assertEquals(3, dice.getIndexFor("Hit"));
        assertEquals(6, dice.getIndexFor("Critical"));
    }

    @Test
    public void getResult() throws DiceRollerException {
        Bucket bucket = new Bucket();
        bucket.setRandomizer(new CustomRandomizer());
        bucket.parseQuery("4k6");
        assertNotNull(bucket.getDice());
        assertEquals(Integer.class, bucket.getDice().getClazz());
        Dice dice = bucket.getDice();
        assertEquals(6, dice.getSides());

        Result result = bucket.getResult();
        assertNotNull(result);
        Queue results = result.getRawResults();
        assertEquals(2, results.remove());
        assertEquals(1, results.remove());
        assertEquals(6, results.remove());
        assertEquals(5, results.remove());
    }

    @Test
    public void getResultCustomNumeric() throws DiceRollerException {
        Bucket bucket = new Bucket();
        bucket.setRandomizer(new CustomRandomizer());
        bucket.parseQuery("12custom[2;3;4;4;6;6;8]");
        assertNotNull(bucket.getDice());
        assertEquals(Integer.class, bucket.getDice().getClazz());
        Dice dice = bucket.getDice();
        assertEquals(7, dice.getSides());

        Result result = bucket.getResult();
        assertNotNull(result);
        Queue results = result.getRawResults();
        assertEquals(3, results.remove());
        assertEquals(2, results.remove());
        assertEquals(6, results.remove());
        assertEquals(6, results.remove());
    }

    @Test
    public void getResultTextual() throws DiceRollerException {
        Bucket bucket = new Bucket();
        bucket.setRandomizer(new CustomRandomizer());
        bucket.parseQuery("4custom{Blank;2:Hit;2:Focus;Critical}");
        assertNotNull(bucket.getDice());
        assertEquals(String.class, bucket.getDice().getClazz());
        Dice dice = bucket.getDice();
        assertEquals(6, dice.getSides());

        Result result = bucket.getResult();
        assertNotNull(result);
        Queue results = result.getRawResults();
        assertEquals("Hit", results.remove());
        assertEquals("Blank", results.remove());
        assertEquals("Critical", results.remove());
        assertEquals("Focus", results.remove());
    }

    @Test
    public void rollCustomDicesExplodedInversed() {
        makeBucket("3custom[0;2:1;2:2;3]ir");
        assertResult(1, 0, 3, 2);
    }

    @Test
    public void rollCustomDicesExplodedRecursive() {
        makeBucket("4custom[0;2:1;2:2;3]rr2");
        assertResult(1, 0, 3, 2, 1, 2, 3, 1);
    }

    // {2, 1, 6, 5, 3, 4, 6, 3, 1, 5, 2, 4};
    // {1, 0, 3, 2, 1, 2, 3, 1, 0, 2, 1, 2};
    @Test
    public void rollCustomDicesExplodedRecursiveInversed() {
        makeBucket("3custom[0;2:1;2:2;3]irr1");
        assertResult(1, 0, 3, 2, 1, 2);
    }

    //explode filter
    @Test
    public void testExplodeFilter() {
        makeBucket("4k6r");
        assertEquals(4, result.getRawResults().size());
        assertEquals(5, result.getProcessedResults().size());

        assertResult(2, 1, 6, 5, 3);
    }

    @Test
    public void testExplodeFilterCustomNumbered() {
        makeBucket("5custom[0;2:1;2:2;3]r");
        assertResult(1, 0, 3, 2, 1, 2);
    }

    @Test
    public void testExplodeXWing() {
        makeXWingBucket(5, "r");
        assertResult("Hit", "Blank", "Critical", "Focus", "Hit", "Focus");
    }

    @Test
    public void rollAndExplodeRecursive() {
        makeXWingBucket(5, "rr4");
        assertResult("Hit", "Blank", "Critical", "Focus", "Hit", "Focus", "Critical", "Hit", "Blank");
    }

    @Test
    public void rollAndExplodeRecursiveNamed() {
        makeXWingBucket(5, "rr[Focus]");
        assertResult("Hit", "Blank", "Critical", "Focus", "Hit", "Focus", "Critical", "Hit", "Blank");
    }

    @Test
    public void rollAndExplodeInversed() {
        makeXWingBucket(5, "ir");
        assertResult("Hit", "Blank", "Critical", "Focus", "Hit", "Focus");
    }

    @Test
    public void rollAndExplodeInversedWithBoundaries() {
        makeXWingBucket(5, "ir2");
        assertResult("Hit", "Blank", "Critical", "Focus", "Hit", "Focus", "Critical", "Hit");
    }

    @Test
    public void rollAndExplodeInversedWithBoundariesNamed() {
        makeXWingBucket(5, "ir[Hit]");
        assertResult("Hit", "Blank", "Critical", "Focus", "Hit", "Focus", "Critical", "Hit");
    }

    @Test
    public void rollAndExplodeInversedReversedWithBoundaries() {
        makeXWingBucket(5, "irr2");
        assertResult("Hit", "Blank", "Critical", "Focus", "Hit", "Focus", "Critical", "Hit", "Blank", "Focus");
    }

    @Test
    public void rollAndExplodeInversedReversedWithBoundariesNamed() {
        makeXWingBucket(5, "irr[Hit]");
        assertResult("Hit", "Blank", "Critical", "Focus", "Hit", "Focus", "Critical", "Hit", "Blank", "Focus");
    }


    /*
        BRANCH
     */
    @Test
    public void rollDiceWithBranch() {
        makeBucket("3d6b");
        assertResult(2, 1, 6, 6);
    }

    @Test
    public void rollDiceWithBranchInversed() {
        makeBucket("3d6ib");
        assertResult(2, 1, 6, 1);
    }

    @Test
    public void rollDiceWithBranchWithBoundary() {
        makeBucket("4d6b5");
        assertResult(2, 1, 6, 5, 6, 5);
    }

    @Test
    public void rollDiceWithBranchInversedWithBoundary() {
        makeBucket("3d6ib2");
        assertResult(2, 1, 6, 2, 1);
    }

    @Test
    public void rollCustomDiceWithBranchWithBoundary() {
        makeBucket("4custom[0;2:1;2:2;3]b2");
        assertResult(1, 0, 3, 2, 3, 2);
    }

    @Test
    public void rollTextualDiceWithBranch() {
        makeXWingBucket(3, "b");
        assertResult("Hit", "Blank", "Critical", "Critical");
    }

    @Test
    public void rollTextualDiceWithBranchInversed() {
        makeXWingBucket(3, "ib");
        assertResult("Hit", "Blank", "Critical", "Blank");
    }

    @Test
    public void rollTextualDiceWithBranchWithBoundary() {
        makeXWingBucket(4, "b5");
        assertResult("Hit", "Blank", "Critical", "Focus", "Critical", "Focus");
    }

    @Test
    public void rollTextualDiceWithBranchInversedWithBoundary() {
        makeXWingBucket(3, "ib2");
        assertResult("Hit", "Blank", "Critical", "Hit", "Blank");
    }

    @Test
    public void rollTextualDiceWithBranchWithBoundaryNamed() {
        makeXWingBucket(4, "b[Focus]");
        assertResult("Hit", "Blank", "Critical", "Focus", "Critical", "Focus");
    }

    @Test
    public void rollTextualDiceWithBranchInversedWithBoundaryNamed() {
        makeXWingBucket(3, "ib[Hit]");
        assertResult("Hit", "Blank", "Critical", "Hit", "Blank");
    }


    private void makeXWingBucket(int dices, String suffix) {
        String query = "" + dices + "custom{Blank;2:Hit;2:Focus;Critical}" + suffix;
        makeBucket(query);
    }

    /*
    Sort
     */
    @Test
    public void rollDiceAndSort() {
        makeBucket("6k6s");
        assertResult(1, 2, 3, 4, 5, 6);
    }

    @Test
    public void rollDiceAndSortInversed() {
        makeBucket("6k6is");
        assertResult(6, 5, 4, 3, 2, 1);
    }

    @Test
    public void rollCustomDiceAndSort() {
        makeBucket("4custom[0;2:1;2:2;3]s");
        assertResult(0, 1, 2, 3);
    }

    @Test
    public void rollCustomDiceAndSortInversed() {
        makeBucket("4custom[0;2:1;2:2;3]is");
        assertResult(3, 2, 1, 0);
    }

    @Test
    public void rollXWingDiceAndSort() {
        makeXWingBucket(5, "s");
        assertResult("Blank", "Hit", "Hit", "Focus", "Critical");
    }


    @Test
    public void rollXwingDiceAndSortInversed() {
        makeXWingBucket(5, "is");
        assertResult("Critical", "Focus", "Hit", "Hit", "Blank");
    }

    @Test
    public void rollAndAdd() {
        makeBucket("5k6+1");
        assertResult(3, 2, 6, 6, 4);
    }

    @Test
    public void rollAndAddAdd() {
        makeBucket("5k6++2");
        assertResult(4, 3, 8, 7, 5);
    }

    @Test
    public void rollAndSubstract() {
        makeBucket("5k6-2");
        assertResult(1, 1, 4, 3, 1);
    }

    @Test
    public void rollAndSubstractTwice() {
        makeBucket("5k6--1");
        assertResult(1, 0, 5, 4, 2);
    }

    @Test
    public void rollAndMultiply() {
        makeBucket("6k6*2");
        assertResult(4, 2, 12, 10, 6, 8);
    }

    //highest
    @Test
    public void returnHigherResult() {
        makeBucket("6k6h");
        assertResult(6);
    }

    @Test
    public void returnHigherResultSmallerRange() {
        makeBucket("2k6h");
        assertResult(2);
    }

    @Test
    public void returnFourHigherResults() {
        makeBucket("6k6h4");
        assertResult(6, 5, 3, 4);
    }

    @Test
    public void dropHigherResult() {
        makeBucket("6k6ih");
        assertResult(2, 1, 5, 3, 4);
    }

    @Test
    public void dropTwoHigherResult() {
        makeBucket("6k6ih2");
        assertResult(2, 1, 3, 4);
    }

    //lower
    @Test
    public void rollAndLeaveLowest() {
        makeBucket("5k6l");
        assertResult(1);

    }

    @Test
    public void rollAndLeaveThreeLowest() {
        makeBucket("6k6l3");
        assertResult(2, 1, 3);
    }

    @Test
    public void rollAndDropLowest() {
        makeBucket("5k6il");
        assertResult(2, 6, 5, 3);
    }

    @Test
    public void rollAndDropThreeLowest() {
        makeBucket("7k6il3");
        assertResult(6, 5, 4, 6);
    }

    //highest
    @Test
    public void returnHigherTextualResult() {
        makeXWingBucket(5, "h");
        assertResult("Critical");
    }

    @Test
    public void returnHigherTextualResultSmallerRange() {
        makeXWingBucket(2, "h");
        assertResult("Hit");
    }

    @Test
    public void returnTextualFourHigherResults() {
        makeXWingBucket(5, "h4");
        assertResult("Hit", "Critical", "Focus", "Hit");
    }

    @Test
    public void dropHigherResultTextual() {
        makeXWingBucket(6, "ih");
        assertResult("Hit", "Blank", "Focus", "Hit", "Focus");
    }
    //        final int[] exp = {Hit, Blank, Critical, Focus, Hit, Focus, Critical, Hit, Blank, Focus, Hit, Focus};

    @Test
    public void dropTwoHigherTextualResult() {
        makeXWingBucket(6, "ih2");
        assertResult("Hit", "Blank", "Hit", "Focus");
    }

    @Test
    public void rollAndLeaveTextualLowest() {
        makeXWingBucket(5, "l");
        assertResult("Blank");

    }

    @Test
    public void rollAndLeaveThreeTextualLowest() {
        makeXWingBucket(6, "l3");
        assertResult("Hit", "Blank", "Hit");
    }

    @Test
    public void rollAndLeaveFourLowest() {
        makeXWingBucket(6, "l4");
        assertResult("Hit", "Blank", "Hit", "Focus");
    }

    @Test
    public void rollAndDropLowestTextual() {
        makeXWingBucket(5, "il");
        assertResult("Hit", "Critical", "Focus", "Hit");

    }

    @Test
    public void rollAndDropThreeLowestTextual() {
        makeXWingBucket(7, "il3");
        assertResult("Critical", "Focus", "Focus", "Critical");
    }

    @Test
    public void countPresenter() {
        makeBucket("6k6c");
        assertPresenter(1);
    }

    @Test
    public void countPresenter2() {
        makeBucket("7k6c");
        assertPresenter(2);

    }

    @Test
    public void countPresenterWithBoundaries() {
        makeBucket("7k6c5");
        assertPresenter(3);
    }

    @Test
    public void countInversedPresenter() {
        makeBucket("6k6ic");
        assertPresenter(1);
    }

    @Test
    public void countInversedPresenterWithBoundary() {
        makeBucket("6k6ic2");
        assertPresenter(2);
    }

    @Test
    public void countPresenterCustomDice() {
        makeBucket("6custom[0;2:1;2:2;3]c2");
        assertPresenter(3);
    }

    @Test
    public void countPresenterInversedCustomDice() {
        makeBucket("6custom[0;2:1;2:2;3]ic");
        assertPresenter(1);
    }

    @Test
    public void countPresenterInversedCustomDiceWithBoundary() {
        makeBucket("6custom[0;2:1;2:2;3]ic1");
        assertPresenter(3);
    }

    @Test
    public void countPresenterTextual() {
        makeXWingBucket(6, "c");
        assertPresenter(1);
    }

    @Test
    public void countPresenter2Textual() {
        makeXWingBucket(7, "c");
        assertPresenter(2);

    }

    @Test
    public void countPresenterWithBoundariesTextual() {
        makeXWingBucket(7, "c5");
        assertPresenter(4);
    }

    @Test
    public void countPresenterWithBoundariesTextualNew() {
        makeXWingBucket(7, "c[Focus]");
        assertPresenter(4);
    }

    @Test
    public void countInversedPresenterTextual() {
        makeXWingBucket(6, "ic");
        assertPresenter(1);
    }

    @Test
    public void countInversedPresenterWithBoundaryTextual() {
        makeXWingBucket(6, "ic2");
        assertPresenter(3);
    }

    @Test
    public void countInversedPresenterWithBoundaryTextual2() {
        makeXWingBucket(6, "ic[Hit]");
        assertPresenter(3);
    }

    @Test
    public void maxPresenterT() {
        makeXWingBucket("m");
        assertPresenter("Hit");

    }

    @Test
    public void maxPresenterFirstT() {
        makeXWingBucket(8, "m");
        assertPresenter("Hit");
    }

    @Test
    public void maxPresenterMultipleT() {
        makeXWingBucket(6, "mm");
        assertPresenter("Hit", "Focus");
    }

    @Test
    public void minPresenterT() {
        makeXWingBucket(7, "im");
        assertPresenter("Blank");
    }

    @Test
    public void minPresenterFirstT() {
        makeXWingBucket(8, "imm");
        assertPresenter("Blank");
    }

    @Test
    public void minPresenterFirstT2() {
        makeXWingBucket(9, "imm");
        assertPresenter("Blank", "Critical", "Focus");
    }

    @Test
    public void sumPresenter() {
        makeBucket("6k6=");
        assertPresenter(21);
    }

    @Test
    public void sumPresenter2() {
        makeBucket("5k6=");
        assertPresenter(17);
    }

    @Test
    public void sumPresenterCustomDice() {
        makeBucket("5custom[0;2:1;2:2;3]=");
        assertPresenter(7);
    }

    @Test
    public void maxPresenter() {
        makeBucket("7k6m");
        assertPresenter(6);

    }

    @Test
    public void maxPresenterFirst() {
        makeBucket("8k6m");
        assertPresenter(6);
    }

    @Test
    public void maxPresenterMultiple() {
        makeBucket("8k6mm");
        assertPresenter(6, 3);
    }

    @Test
    public void maxPresenterCustom() {
        makeBucket("6custom[0;2:1;2:2;3]m");
        assertPresenter(1);
    }

    @Test
    public void maxPresenterMultipleCustom() {
        makeBucket("6custom[0;2:1;2:2;3]mm");
        assertPresenter(1, 2);
    }

    @Test
    public void minPresenter() {
        makeBucket("7k6im");
        assertPresenter(2);
    }

    @Test
    public void minPresenterFirst() {
        makeBucket("8k6imm");
        assertPresenter(2, 1, 5, 4);
    }

    @Test
    public void minPresenterCustom() {
        makeBucket("7custom[0;2:1;2:2;3]im");
        assertPresenter(0);
    }

    @Test
    public void minPresenterMultipleCustom() {
        makeBucket("6custom[0;2:1;2:2;3]imm");
        assertPresenter(0, 3);
    }


    //Few complex examples
    @Test
    public void explodeAndBranch() {
        makeBucket("6k6rb");
        assertResult(2, 1, 6, 5, 3, 4, 6, 6, 6);
    }

    public void branchAndExplode() {
        makeBucket("6k6rb");
        assertResult(2, 1, 6, 5, 3, 4, 6, 3, 1);
    }

    @Test
    public void explodeAndBranchCountfrom5() {
        makeBucket("6k6rbc5");
        assertResult(2, 1, 6, 5, 3, 4, 6, 6, 6);
        assertPresenter(5);
    }

    //
//      custom textual:
//      h[Hit] //todo
//      l[Hit] //todo

    // {2, 1, 6, 5, 3, 4, 6, 3, 1, 5, 2, 4};
// {Hit, Blank, Critical, Focus, Hit, Focus, Critical, Hit, Blank, Focus, Hit, Focus}
    private void makeXWingBucket(String suffix) {
        makeXWingBucket(6, suffix);
    }

    private void makeBucket(String query) {
        bucket = new Bucket();
        bucket.setRandomizer(new CustomRandomizer());
        try {
            bucket.parseQuery(query);
        } catch (DiceRollerException e) {
            System.out.println("Wrong query to make bucket!");
        }
        this.result = bucket.getResult();
    }

    private void assertPresenter(Object... expectations) {
        assertEquals(expectations.length, result.getEndResults().size());
        for (Object o : expectations) {
            assertEquals(o, result.getEndResults().remove());
        }
    }


    private void assertResult(Object... expectations) {
        assertEquals(expectations.length, result.getProcessedResults().size());
        for (Object o : expectations) {
            assertEquals(o, result.getProcessedResults().remove());
        }
    }


}
