package net.fivedots.diceroller;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 26.05.2014
 * Time: 14:11
 */
public class DiceTest {


    @Test
    public void basicDice() {
        Dice<String> sDice = new Dice<>(String.class);

        assertEquals(String.class, sDice.getClazz());
        assertEquals(0, sDice.getSides());
    }

    @Test
    public void createDice() throws DiceRollerException {
        Bucket bucket = new Bucket();
        bucket.parseQuery("3k6");

        Dice dice = bucket.getDice();
        assertNotNull(dice);

        assertEquals(Integer.class, dice.getClazz());

        assertEquals(6, dice.getSides());

        assertEquals(1, dice.getElementAtPosition(1));
        assertEquals(3, dice.getElementAtPosition(3));
        assertEquals(5, dice.getElementAtPosition(5));

        assertEquals(0, dice.getIndexFor(1));
        assertEquals(2, dice.getIndexFor(3));
        assertEquals(4, dice.getIndexFor(5));

        assertEquals(1, dice.getPositionFor(1));
        assertEquals(3, dice.getPositionFor(3));
        assertEquals(5, dice.getPositionFor(5));

    }

    @Test
    public void createDiceCustom() throws DiceRollerException {
        Bucket bucket = new Bucket();
        bucket.parseQuery("3custom[1;2:2;3:3]");

        Dice dice = bucket.getDice();
        assertNotNull(dice);

        assertEquals(Integer.class, dice.getClazz());

        assertEquals(6, dice.getSides());

        assertEquals(1, dice.getElementAtPosition(1));
        assertEquals(2, dice.getElementAtPosition(3));
        assertEquals(3, dice.getElementAtPosition(5));

        assertEquals(0, dice.getIndexFor(1));
        assertEquals(1, dice.getIndexFor(2));
        assertEquals(3, dice.getIndexFor(3));

        assertEquals(1, dice.getPositionFor(1));
        assertEquals(2, dice.getPositionFor(2));
        assertEquals(4, dice.getPositionFor(3));

    }

    @Test
    public void createDiceCustomTextual() throws DiceRollerException {
        Bucket bucket = new Bucket();
        bucket.parseQuery("3custom{Blank;Focus;2:Hit;Critical}");

        Dice dice = bucket.getDice();
        assertNotNull(dice);

        assertEquals(String.class, dice.getClazz());

        assertEquals(5, dice.getSides());

        assertEquals("Blank", dice.getElementAtPosition(1));
        assertEquals("Hit", dice.getElementAtPosition(3));
        assertEquals("Critical", dice.getElementAtPosition(5));

        assertEquals(0, dice.getIndexFor("Blank"));
        assertEquals(2, dice.getIndexFor("Hit"));
        assertEquals(4, dice.getIndexFor("Critical"));


        assertEquals(1, dice.getPositionFor("Blank"));
        assertEquals(3, dice.getPositionFor("Hit"));
        assertEquals(5, dice.getPositionFor("Critical"));

    }

}
