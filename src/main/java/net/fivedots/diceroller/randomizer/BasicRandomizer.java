package net.fivedots.diceroller.randomizer;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 15.05.2014
 * Time: 11:30
 */
public class BasicRandomizer implements Randomizer {

    @Override
    public int rollDice(int max) {
        return new Random().nextInt(max) + 1;
    }
}
