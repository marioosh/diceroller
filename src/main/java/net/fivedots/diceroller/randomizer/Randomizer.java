package net.fivedots.diceroller.randomizer;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 15.05.2014
 * Time: 11:16
 */
public interface Randomizer {

    public int rollDice(int max);
}
