package net.fivedots.diceroller;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 26.05.2014
 * Time: 21:25
 */
public class Result<T> {
// ------------------------------ FIELDS ------------------------------

    private Class<T> clazz;

    private Queue<T> rawResults;

    private Queue<T> processedResults;

    private Queue<T> endResults;

    private String rawQuery = "";

    public Result(Class<T> clazz) {
        this.clazz = clazz;
        rawResults = new LinkedList<>();
        processedResults = new LinkedList<>();
        endResults = new LinkedList<>();
    }

    public String getRawQuery() {
        return rawQuery;
    }
    // --------------------------- CONSTRUCTORS ---------------------------

    public void setRawQuery(String rawQuery) {
        this.rawQuery = rawQuery;
    }

    public void addValue(T value) {
        rawResults.offer(value);
        processedResults.offer(value);
    }
// --------------------- GETTER / SETTER METHODS ---------------------

    public Class<T> getClazz() {
        return clazz;
    }

    public Queue<T> getRawResults() {
        return rawResults;
    }

    public void setRawResults(Queue<T> rawResults) {
        this.rawResults = rawResults;
        this.processedResults = rawResults;
    }

    public Queue<T> getProcessedResults() {
        return processedResults;
    }

    public void setProcessedResults(Queue<T> processedResults) {
        this.processedResults = processedResults;
    }

    public Queue<T> getEndResults() {
        return endResults;
    }
}
