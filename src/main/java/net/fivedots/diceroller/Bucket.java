package net.fivedots.diceroller;

import net.fivedots.diceroller.filter.Filter;
import net.fivedots.diceroller.formatter.BasicFormatter;
import net.fivedots.diceroller.formatter.Formatter;
import net.fivedots.diceroller.randomizer.BasicRandomizer;
import net.fivedots.diceroller.randomizer.Randomizer;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 26.05.2014
 * Time: 14:06
 */
public class Bucket {
// ------------------------------ FIELDS ------------------------------

    private Dice dice;

    private int numberOfDices;

    private Randomizer randomizer;

    private Result result;

    private List<Filter> filters;

    private String rawQuery = "";

// --------------------------- CONSTRUCTORS ---------------------------

    public Bucket() {
        filters = Filter.getAll();
        randomizer = new BasicRandomizer();
    }

// --------------------- GETTER / SETTER METHODS ---------------------

    public Dice getDice() {
        return dice;
    }

    public void setDice(Dice dice) {
        this.dice = dice;
    }

    public int getNumberOfDices() {
        return numberOfDices;
    }

    public void setNumberOfDices(int numberOfDices) {
        this.numberOfDices = numberOfDices;
    }

    public Randomizer getRandomizer() {
        return randomizer;
    }

    public void setRandomizer(Randomizer randomizer) {
        this.randomizer = randomizer;
    }

    public String getRawQuery() {
        return rawQuery;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

// -------------------------- OTHER METHODS --------------------------

    public void parseQuery(String query) throws DiceRollerException {
        this.rawQuery = query;

        StringBuilder sb = new StringBuilder(query);

        queryLoop:
        while (sb.length() > 0) {
            for (Filter filter : filters) {
                if (filter.match(sb.toString())) {
                    filter.process(this, sb);
                    filter.cleanQuery(sb);
                    filters.remove(filter);
                    continue queryLoop;
                }
            }
            throw new ParseException("Unidentified pattern");
        }
    }

    @SuppressWarnings("unchecked")
    public void roll(Result result) {
        for (int i = 0; i < getNumberOfDices(); i++) {
            if (!getDice().getElements().isEmpty()) {
                result.addValue(getDice().getElementAtPosition(randomizer.rollDice(getDice().getSides())));
            } else {
                result.addValue(randomizer.rollDice(getDice().getSides()));
            }
        }
        result.setRawQuery(rawQuery);
        setResult(result);
    }

    public String format() {
        return new BasicFormatter().format(getResult());
    }

    public String format(Formatter formatter) {
        return formatter.format(getResult());
    }
}
