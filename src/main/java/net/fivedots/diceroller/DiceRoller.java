package net.fivedots.diceroller;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 26.05.2014
 * Time: 14:05
 */
public class DiceRoller {

    public static void main(String[] args) {
        DiceRoller roller = new DiceRoller();
        System.out.println(roller.roll("5k6ic5"));
    }

    public String roll(String query) {
        Bucket bucket = new Bucket();
        bucket.parseQuery(query);
        return bucket.format();
    }
}
