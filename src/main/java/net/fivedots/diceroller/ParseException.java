package net.fivedots.diceroller;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 12.05.2014
 * Time: 22:32
 */
public class ParseException extends DiceRollerException {
    public ParseException(String s) {
        super(s);

    }
}
