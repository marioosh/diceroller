package net.fivedots.diceroller;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 26.05.2014
 * Time: 14:08
 */
public class Dice<T> {
// ------------------------------ FIELDS ------------------------------

    private Class<T> clazz;

    private int sides = 0;

    private Queue<T> elements = null;

// --------------------------- CONSTRUCTORS ---------------------------

    public Dice(Class<T> clazz) {
        this(clazz, 0);
    }

    public Dice(Class<T> clazz, int sides) {
        this.clazz = clazz;
        this.sides = sides;
        elements = new LinkedList<>();
    }

// --------------------- GETTER / SETTER METHODS ---------------------

    public Class<T> getClazz() {
        return clazz;
    }

    public Queue<T> getElements() {
        return elements;
    }

    public int getSides() {
        if (!elements.isEmpty())
            return elements.size();
        return sides;
    }

// -------------------------- OTHER METHODS --------------------------

    public void addValue(T value) {
        elements.offer(value);
    }

    @SuppressWarnings("unchecked")
    public T getElementAtPosition(int position) {
        if (!elements.isEmpty()) {
            return ((List<T>) elements).get(position - 1);
        } else {
            return (T) new Integer(position);
        }
    }

    @SuppressWarnings("unchecked")
    public int getIndexFor(T value) {
        if (!elements.isEmpty()) {
            return ((List<T>) elements).indexOf(value);
        } else {
            //if not its basic numbered dice
            //index is value -1
            try {
                int v = (Integer) value;
                return v - 1;
            } catch (ClassCastException cce) {
                throw new DiceRollerException("Cannot find value");
            }
        }
    }

    public int getPositionFor(T value) {
        return getIndexFor(value) + 1;
    }
}
