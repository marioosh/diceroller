package net.fivedots.diceroller.filter;

import net.fivedots.diceroller.Bucket;
import net.fivedots.diceroller.Dice;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 26.05.2014
 * Time: 14:41
 */
@SuppressWarnings("unchecked")
public class ExplodeFilter extends Filter {
// ------------------------------ FIELDS ------------------------------

    private boolean inversed = false;

    private boolean recursive = false;

    private int boundary = 0;

// --------------------------- CONSTRUCTORS ---------------------------

    public ExplodeFilter() {
        setMatcher("i?r?r\\d*");
    }

// -------------------------- OTHER METHODS --------------------------

    @Override
    public void process(Bucket bucket, StringBuilder query) {
        if (getFoundString().startsWith("i"))
            inversed = true;
        if (getFoundString().contains("rr"))
            recursive = true;
        //boundary
        Pattern p = Pattern.compile("r(\\d+)");
        Matcher m = p.matcher(getFoundString());
        if (m.find()) {
            try {
                boundary = Integer.parseInt(m.group(1));
            } catch (NumberFormatException ignored) {
            }
        } else {
            if (bucket.getDice().getElements().isEmpty() || bucket.getDice().getClazz().equals(String.class)) {
                boundary = inversed ? 1 : bucket.getDice().getSides();
            } else {

                if (inversed) {
                    boundary = (int) Collections.min(bucket.getDice().getElements());
                } else {
                    boundary = (int) Collections.max(bucket.getDice().getElements());
                }

            }
        }
        //start processing
        Queue rerolls = new LinkedList();
        for (Object obj : bucket.getResult().getProcessedResults()) {
            if (checkRoll(obj, bucket.getDice())) {
                if (recursive) {
                    rerolls.addAll(rerollRecursive(bucket));
                } else {
                    rerolls.add(rollDice(bucket));
                }

            }
        }

        bucket.getResult().getProcessedResults().addAll(rerolls);
    }

    private Object rollDice(Bucket bucket) {
        return bucket.getDice().getElementAtPosition(bucket.getRandomizer().rollDice(bucket.getDice().getSides()));
    }

    @SuppressWarnings("unchecked")
    private Queue rerollRecursive(Bucket bucket) {
        Queue rerolls = new LinkedList();

        Object reroll = rollDice(bucket);
        rerolls.add(reroll);
        if (checkRoll(reroll, bucket.getDice())) {
            rerolls.addAll(rerollRecursive(bucket));
        }

        return rerolls;
    }

    /**
     * If it's Integer value check value against boundary
     * if it's String value, check position at elements.list
     */
    private boolean checkRoll(Object obj, Dice dice) {
        if (dice.getClazz().equals(Integer.class)) {
            int index = (Integer) obj;
            return inversed ? index <= boundary : index >= boundary;

        } else if (dice.getClazz().equals(String.class)) {
            int index = dice.getPositionFor(obj);
            return inversed ? index <= boundary : index >= boundary;
        }
        return false;

    }


}
