package net.fivedots.diceroller.filter;

import net.fivedots.diceroller.Bucket;
import net.fivedots.diceroller.DiceRollerException;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 27.05.2014
 * Time: 11:42
 */
public class SortFilter extends Filter {
    private boolean inversed = false;

    public SortFilter() {
        setMatcher("i?s");
    }

    @Override
    @SuppressWarnings("unchecked")
    public void process(Bucket bucket, StringBuilder query) throws DiceRollerException {
        if (getFoundString().startsWith("i"))
            inversed = true;
        //process

        if (bucket.getDice().getClazz().equals(Integer.class)) {


            if (inversed) {
                Collections.sort((java.util.List<Comparable>) bucket.getResult().getProcessedResults());
                Collections.reverse((java.util.List<Comparable>) bucket.getResult().getProcessedResults());
            } else {
                Collections.sort((java.util.List<Comparable>) bucket.getResult().getProcessedResults());
            }
        } else if (bucket.getDice().getClazz().equals(String.class)) {

            Queue<String> sorted = new LinkedList<>();
            List<Integer> resIdx = new ArrayList<>();
            for (Object object : bucket.getResult().getProcessedResults()) {
                String s = (String) object;
                resIdx.add(bucket.getDice().getIndexFor(s));
            }
            Collections.sort(resIdx);
            if (inversed) Collections.reverse(resIdx);
            for (Integer i : resIdx) {
                sorted.add((String) bucket.getDice().getElementAtPosition(i + 1));
            }

            bucket.getResult().setProcessedResults(sorted);

        }
    }
}
