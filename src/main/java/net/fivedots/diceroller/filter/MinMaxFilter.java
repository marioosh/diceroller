package net.fivedots.diceroller.filter;

import net.fivedots.diceroller.Bucket;
import net.fivedots.diceroller.DiceRollerException;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 28.05.2014
 * Time: 09:24
 */
public class MinMaxFilter extends Filter {

    private boolean inversed = false;

    private boolean multiple = false;

    public MinMaxFilter() {
        setMatcher("i?mm?");
    }

    @Override
    public void process(Bucket bucket, StringBuilder query) throws DiceRollerException {
        if (getFoundString().startsWith("i")) inversed = true;
        if (getFoundString().contains("mm")) multiple = true;

        LinkedHashMap<Object, Integer> mapping = new LinkedHashMap<>();
        for (Object record : bucket.getResult().getProcessedResults()) {
            int value = mapping.containsKey(record) ? mapping.get(record) + 1 : 1;
            mapping.put(record, value);
        }

        int target = 0;
        if (inversed) {
            target = Collections.min(mapping.values());
        } else {
            target = Collections.max(mapping.values());
        }
        //clean end results
        bucket.getResult().getEndResults().clear();

        for (Map.Entry<Object, Integer> entry : mapping.entrySet()) {
            if (entry.getValue() == target) {
                bucket.getResult().getEndResults().offer(entry.getKey());
                if (!multiple) return;
            }
        }

    }
}
