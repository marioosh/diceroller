package net.fivedots.diceroller.filter;

import net.fivedots.diceroller.Bucket;
import net.fivedots.diceroller.Dice;
import net.fivedots.diceroller.DiceRollerException;
import net.fivedots.diceroller.Result;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 26.05.2014
 * Time: 14:37
 */
public class NumericDiceFilter extends Filter {
// --------------------------- CONSTRUCTORS ---------------------------

    public NumericDiceFilter() {
        setMatcher("(\\d)*[d|k](\\d)+");
    }

// -------------------------- OTHER METHODS --------------------------

    @Override
    public void process(Bucket bucket, StringBuilder query) throws DiceRollerException {
        int dices = 1;
        int sides = 0;


        final String pattern = "^" + getMatcher();
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(query);
        if (m.find()) {
            try {
                if (m.group(1) != null)
                    dices = Integer.parseInt(m.group(1));
                sides = Integer.parseInt(m.group(2));
            } catch (NumberFormatException nfe) {
                throw new DiceRollerException("Cannot read dice definition");
            }
//            setFoundString(m.group());
            //delete found fragment
//            query.delete(0, getFoundString().length());
        }

        Dice<Integer> dice = new Dice<>(Integer.class, sides);
        bucket.setDice(dice);
        bucket.setNumberOfDices(dices);

        //set result and roll dices
        Result<Integer> result = new Result<>(Integer.class);
        bucket.roll(result);
    }

}
