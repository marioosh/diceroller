package net.fivedots.diceroller.filter;

import net.fivedots.diceroller.Bucket;
import net.fivedots.diceroller.DiceRollerException;

import java.util.LinkedList;
import java.util.Queue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 27.05.2014
 * Time: 12:12
 */
public class MultiplyFilter extends Filter {
    private int modifier = 0;

    public MultiplyFilter() {
        setMatcher("\\*\\d+");
    }

    @Override
    public void process(Bucket bucket, StringBuilder query) throws DiceRollerException {
        if (bucket.getDice().getClazz().equals(Integer.class)) {

            Pattern p = Pattern.compile("(\\d+)");
            Matcher matcher = p.matcher(getFoundString());
            if (matcher.find()) {
                try {
                    modifier = Integer.parseInt(matcher.group(1));

                } catch (NumberFormatException ignored) {

                }
            }
            //process

            Queue<Integer> results = new LinkedList<>();

            final int size = bucket.getResult().getProcessedResults().size();
            for (int i = 0; i < size; i++) {
                int current = (int) bucket.getResult().getProcessedResults().remove();

                results.add(current * modifier);
            }
            bucket.getResult().setProcessedResults(results);

        }
    }
}
