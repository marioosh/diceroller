package net.fivedots.diceroller.filter;

import net.fivedots.diceroller.Bucket;
import net.fivedots.diceroller.DiceRollerException;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 26.05.2014
 * Time: 14:16
 */
public abstract class Filter {
    private String matcher;

    private String foundString = "";

    public static List<Filter> getAll() {
        List<Filter> filters = new ArrayList<>();

        filters.add(new NumericDiceFilter());
        filters.add(new CustomNumericDiceFilter());
        filters.add(new CustomTextualDiceFilter());
        filters.add(new NamedExplodeFilter());
        filters.add(new ExplodeFilter());
        filters.add(new NamedBranchFilter());
        filters.add(new BranchFilter());
        filters.add(new SortFilter());
        filters.add(new AddSubstractFilter());
        filters.add(new MultiplyFilter());
        filters.add(new HighestLowestFilter());
        filters.add(new NamedCountFilter());
        filters.add(new CountFilter());
        filters.add(new SumFilter());
        filters.add(new MinMaxFilter());

        return filters;
    }

    public String getFoundString() {
        return foundString;
    }

    public void setFoundString(String foundString) {
        this.foundString = foundString;
    }

    protected String getMatcher() {
        return matcher;
    }

    protected void setMatcher(String matcher) {
        this.matcher = matcher;
    }

    public boolean match(String query) {
        Pattern pattern = Pattern.compile("^" + matcher);
        Matcher m = pattern.matcher(query);
        if (m.find()) {
            setFoundString(m.group());
            return true;
        }
        return false;
    }

    public abstract void process(Bucket bucket, StringBuilder query) throws DiceRollerException;

    public void cleanQuery(StringBuilder builder) {
        builder.delete(0, getFoundString().length());
    }
}
