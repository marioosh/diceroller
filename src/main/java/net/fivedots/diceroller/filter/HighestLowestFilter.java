package net.fivedots.diceroller.filter;

import net.fivedots.diceroller.Bucket;
import net.fivedots.diceroller.DiceRollerException;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 27.05.2014
 * Time: 13:02
 */
public class HighestLowestFilter extends Filter {
    private boolean keep = true;

    private boolean high = true;

    private int range = 1;

    public HighestLowestFilter() {
        setMatcher("i?[l|h]\\d*");
    }

    @Override
    public void process(Bucket bucket, StringBuilder query) throws DiceRollerException {
        //process

        if (getFoundString().startsWith("i"))
            keep = false;
        if (getFoundString().startsWith("l"))
            high = false;
        if (getFoundString().startsWith("il")) {
            keep = false;
            high = false;
        }
        //
        Pattern p = Pattern.compile("[l|h](\\d+)");
        Matcher m = p.matcher(getFoundString());
        if (m.find()) {
            try {
                range = Integer.parseInt(m.group(1));

            } catch (NumberFormatException ignored) {
            }
        }
        //
        if (keep)
            range = bucket.getResult().getProcessedResults().size() - range;

        //integer first
        if (bucket.getDice().getClazz().equals(Integer.class)) {

            for (int i = 0; i < range; i++) {
                int element = -1;
                if (keep ^ high)
                    element = (int) Collections.max(bucket.getResult().getProcessedResults());
                else
                    element = (int) Collections.min(bucket.getResult().getProcessedResults());
                ((List<Integer>) bucket.getResult().getProcessedResults()).remove(((List<Integer>) bucket.getResult().getProcessedResults()).indexOf(element));
            }

        } else if (bucket.getDice().getClazz().equals(String.class)) {

            Queue<String> elements = sortByDice(bucket, keep ^ high);
            for (int i = 0; i < range; i++) {
                String min = elements.remove();
                ((List<String>) bucket.getResult().getProcessedResults()).remove(((List<String>) bucket.getResult().getProcessedResults()).indexOf(min));
            }

        }
    }

    private Queue<String> sortByDice(Bucket bucket, boolean inversed) {
        Queue<String> sorted = new LinkedList<>();
        List<Integer> resIdx = new ArrayList<>();
        for (Object object : bucket.getResult().getProcessedResults()) {
            String s = (String) object;
            resIdx.add(bucket.getDice().getIndexFor(s));
        }
        Collections.sort(resIdx);
        if (inversed) Collections.reverse(resIdx);
        for (Integer i : resIdx) {
            sorted.add((String) bucket.getDice().getElementAtPosition(i + 1));
        }

        return sorted;
    }
}
