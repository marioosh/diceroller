package net.fivedots.diceroller.filter;

import net.fivedots.diceroller.Bucket;
import net.fivedots.diceroller.Dice;
import net.fivedots.diceroller.DiceRollerException;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 27.05.2014
 * Time: 11:13
 */
@SuppressWarnings("uncheked")
public class NamedBranchFilter extends Filter {
    private boolean inversed;

    private int boundary;

    public NamedBranchFilter() {
        setMatcher("i?b\\[\\w*\\]");
    }

    @Override
    public void process(Bucket bucket, StringBuilder query) throws DiceRollerException {

        if (getFoundString().startsWith("i"))
            inversed = true;
        //boundary
        Pattern p = Pattern.compile("b\\[(\\w*)\\]");
        Matcher m = p.matcher(getFoundString());
        if (m.find()) {
            boundary = bucket.getDice().getPositionFor(m.group(1));

        } else {
            if (bucket.getDice().getElements().isEmpty() || bucket.getDice().getClazz().equals(String.class)) {
                boundary = inversed ? 1 : bucket.getDice().getSides();
            } else {

                if (inversed) {
                    boundary = (int) Collections.min(bucket.getDice().getElements());
                } else {
                    boundary = (int) Collections.max(bucket.getDice().getElements());
                }

            }
        }

        //start processing
        Queue branched = new LinkedList();
        for (Object obj : bucket.getResult().getProcessedResults()) {
            if (checkRoll(obj, bucket.getDice())) {
                branched.offer(obj);
            }
        }

        bucket.getResult().getProcessedResults().addAll(branched);
    }

    /**
     * If it's Integer value check it against boundary
     * if it's String value, check position at elements.list
     */
    private boolean checkRoll(Object obj, Dice dice) {
        if (dice.getClazz().equals(Integer.class)) {
            int index = (Integer) obj;
            return inversed ? index <= boundary : index >= boundary;

        } else if (dice.getClazz().equals(String.class)) {
            int index = dice.getPositionFor(obj);
            return inversed ? index <= boundary : index >= boundary;
        }
        return false;

    }

}
