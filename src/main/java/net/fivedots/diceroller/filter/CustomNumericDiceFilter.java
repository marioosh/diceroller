package net.fivedots.diceroller.filter;

import net.fivedots.diceroller.Bucket;
import net.fivedots.diceroller.Dice;
import net.fivedots.diceroller.Result;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 26.05.2014
 * Time: 14:39
 */
public class CustomNumericDiceFilter extends Filter {
// --------------------------- CONSTRUCTORS ---------------------------

    public CustomNumericDiceFilter() {
        setMatcher("(\\d)*cu(stom)?\\[.*?\\]");
    }

// -------------------------- OTHER METHODS --------------------------

    @Override
    public void process(Bucket bucket, StringBuilder query) {
        int dices = 1;

        //number of dices
        Pattern p = Pattern.compile("^(\\d*)[cu|custom]");
        Matcher m = p.matcher(query);
        if (m.find()) {
            String result = m.group(1);
            try {
                dices = Integer.parseInt(result);
            } catch (NumberFormatException ignored) {
            }
        }

        bucket.setNumberOfDices(dices);
        //process sides
        Dice<Integer> dice = createCustomDice(query.toString());
        bucket.setDice(dice);
        Result<Integer> result = new Result<>(Integer.class);
        bucket.roll(result);

    }

    private Dice<Integer> createCustomDice(String query) {
        Dice<Integer> dice = new Dice<>(Integer.class);
        Pattern p = Pattern.compile("[cu|custom]\\[([\\d;:]*?)\\]");
        Matcher m = p.matcher(query);
        String sidesDefinition = "";
        if (m.find()) {
            sidesDefinition = m.group(1);
        }
        String[] definitions = sidesDefinition.split(";");
        for (String definition : definitions) {
            if (definition.matches("\\d*")) {
                dice.addValue(Integer.parseInt(definition));
            } else if (definition.matches("\\d+:\\d+")) {
                String[] complexDefinition = definition.split(":");
                int counter = Integer.parseInt(complexDefinition[0]);
                int value = Integer.parseInt(complexDefinition[1]);
                for (int i = 0; i < counter; i++) {
                    dice.addValue(value);
                }
            }
        }

        return dice;
    }

}
