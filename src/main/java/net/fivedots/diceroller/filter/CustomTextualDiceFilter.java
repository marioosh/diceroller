package net.fivedots.diceroller.filter;

import net.fivedots.diceroller.Bucket;
import net.fivedots.diceroller.Dice;
import net.fivedots.diceroller.Result;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 26.05.2014
 * Time: 14:38
 */
public class CustomTextualDiceFilter extends Filter {
// --------------------------- CONSTRUCTORS ---------------------------

    public CustomTextualDiceFilter() {
        setMatcher("(\\d)*cu(stom)?\\{.*?\\}");
    }

// -------------------------- OTHER METHODS --------------------------

    @Override
    public void process(Bucket bucket, StringBuilder query) {
        int dices = 1;

        //number of dices
        Pattern p = Pattern.compile("^(\\d*)cu(stom)?");
        Matcher m = p.matcher(query);
        if (m.find()) {
            String result = m.group(1);
            try {
                dices = Integer.parseInt(result);
            } catch (NumberFormatException ignored) {
            }
        }

        bucket.setNumberOfDices(dices);
        //process sides
        Dice<String> dice = createCustomDice(query.toString());
        bucket.setDice(dice);

        //result
        Result<String> result = new Result<>(String.class);
        bucket.roll(result);
    }

    private Dice<String> createCustomDice(String query) {
        Dice<String> dice = new Dice<>(String.class);
        Pattern p = Pattern.compile("[cu|custom]\\{([\\d:;\\w]*)\\}");
        Matcher m = p.matcher(query);
        String sidesDefinition = "";
        if (m.find()) {
            sidesDefinition = m.group(1);
        }
        String[] definitions = sidesDefinition.split(";");
        for (String definition : definitions) {
            if (definition.matches("\\w*")) {
                dice.addValue(definition);
            } else if (definition.matches("\\d*:\\w*")) {
                String[] complexDefinition = definition.split(":");
                int counter = Integer.parseInt(complexDefinition[0]);
                String value = complexDefinition[1];
                for (int i = 0; i < counter; i++) {
                    dice.addValue(value);
                }
            }
        }

        return dice;
    }
}
