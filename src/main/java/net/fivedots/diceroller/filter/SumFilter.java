package net.fivedots.diceroller.filter;

import net.fivedots.diceroller.Bucket;
import net.fivedots.diceroller.DiceRollerException;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 28.05.2014
 * Time: 09:24
 */
public class SumFilter extends Filter {


    public SumFilter() {
        setMatcher("=");
    }

    @Override
    @SuppressWarnings("unchecked")
    public void process(Bucket bucket, StringBuilder query) throws DiceRollerException {
        if (bucket.getDice().getClazz().equals(Integer.class)) {
            Integer sum = 0;
            for (Object value : bucket.getResult().getProcessedResults()) {

                int intValue = (int) value;
                sum += intValue;
            }
            bucket.getResult().getEndResults().clear();
            bucket.getResult().getEndResults().offer(sum);

        }
    }
}
