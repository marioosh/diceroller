package net.fivedots.diceroller.filter;

import net.fivedots.diceroller.Bucket;
import net.fivedots.diceroller.DiceRollerException;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 27.05.2014
 * Time: 12:12
 */
public class AddSubstractFilter extends Filter {
    private boolean substract = false;

    private boolean keepBoundaries = true;

    private int lowBoundary = 0;

    private int highBoundary = 0;

    private int modifier = 0;

    public AddSubstractFilter() {
        setMatcher("[-|+]?[-|+]\\d+");
    }

    @Override
    public void process(Bucket bucket, StringBuilder query) throws DiceRollerException {
        if (bucket.getDice().getClazz().equals(Integer.class)) {
            if (getFoundString().startsWith("-"))
                substract = true;
            if (getFoundString().startsWith("--"))
                keepBoundaries = false;
            if (getFoundString().startsWith("++"))
                keepBoundaries = false;

            if (keepBoundaries) {
                if (bucket.getDice().getElements().isEmpty()) {
                    lowBoundary = 1;
                    highBoundary = bucket.getDice().getSides();
                } else {
                    lowBoundary = (int) Collections.min(bucket.getDice().getElements());
                    highBoundary = (int) Collections.max(bucket.getDice().getElements());
                }
            }


            Pattern p = Pattern.compile("(\\d+)");
            Matcher matcher = p.matcher(getFoundString());
            if (matcher.find()) {
                try {
                    modifier = Integer.parseInt(matcher.group(1));

                } catch (NumberFormatException ignored) {

                }
            }
            //process

            Queue<Integer> results = new LinkedList<>();

            final int size = bucket.getResult().getProcessedResults().size();
            for (int i = 0; i < size; i++) {
                int current = (int) bucket.getResult().getProcessedResults().remove();
                current = substract ? current - modifier : current + modifier;
                if (keepBoundaries) {
                    if (substract) {
                        if (current < lowBoundary) current = lowBoundary;
                    } else {
                        if (current > highBoundary) current = highBoundary;
                    }
                }

                results.add(current);
            }
            bucket.getResult().setProcessedResults(results);

        }
    }
}
