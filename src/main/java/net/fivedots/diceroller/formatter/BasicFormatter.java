package net.fivedots.diceroller.formatter;

import net.fivedots.diceroller.Result;

import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 15.05.2014
 * Time: 10:05
 */
public class BasicFormatter implements Formatter {
    @Override
    public String format(Result result) {
        StringBuilder sb = new StringBuilder();
        //query
        String rawResults = join(result.getRawResults());
        String endResults = join(result.getEndResults().isEmpty() ? result.getProcessedResults() : result.getEndResults());


        sb.append(String.format("|%s| => {%s} = (%s)", result.getRawQuery().trim(), rawResults.trim(), endResults.trim()));
        return sb.toString();
        //
    }

    private String join(Iterable collection) {
        Iterator it = collection.iterator();
        String output = "";
        while (it.hasNext()) {
            Object i = it.next();
            output += i;
            if (it.hasNext())
                output += ", ";
        }
        return output;
    }
}
