package net.fivedots.diceroller.formatter;

import net.fivedots.diceroller.Result;

/**
 * Created with IntelliJ IDEA.
 * User: marioosh
 * Date: 15.05.2014
 * Time: 10:05
 */
public interface Formatter {

    public String format(Result result);
}
