##DiceRoller

**DiceRoller** is java library for customizable dice rolling

###Basic Usage:

Numerical dices:
`d3` - basic result from [1,2,3]

`1d6` - result of 6 sided dice

**d** can be replaced by **k**
`3k6` - Rolls 3d6

`3d10` - 3 results of 10 sided dice

`custom[0;1;1;2;2;3]` - 1 result from custom numeric die

`3custom[5:1;5:2;20:3]` - 3 results from dice that have 30 sides: 5 with result 1, 5 with result 2 and 20 sides with result 3

**Explode**
`3d6r` - 3d6 results, Exploded: on every 6 rolled (a new die is rolled but those new dice don't explode)

`4d8rr` - Rolls 4d8 and "explodes" on every 8 rolled (all newly rolled dice may also further explode)

`3d10rr8` - Rolls 3d10 and explodes on 8,9,10 recursive

`3d6ir` - 3d6 results, Exploded: on every 1 rolled (a new die is rolled but those new dice don't explode)

`3d10irr3` - Rolls 3d10 and explodes on result  1,2,3 recursive

**Branch**
`3d6b` - Rolls 3d6 every 6 is splitted into two dices with 6's

`3d6b5` - Rolls 3d6 every 6 is splitted into two dices with 6, every 5 is splitted into two dices with 5

`3d6ib` - Rolls 3d6 every 1 is splitted into two dices with 1's

`3d6ib2` - Rolls 3d6 every 1 is splitted into two dices with 1, every 2 is splitted into two dices with 2


###Modifiers

You can modify results:

`5d6-3` - Roll 5d6 and substract 3, all computed results lower than 1 are setted as 1

`5d6--3` - Rolls 5d6 and substract 3, computed results can be lower than low boundary (or even 0 and negative values

`4d20+5` - Rolls 4d20 and add 5 to every result, no higher than 20

`4d20++5` - Rolls 4d20 and add 5 to every result, new values can be higher than 20

`3d8*2` - Rolls 3d8 and every result is multiplied 2 times

###Filters:

**s** - sorted results from high to low  
**is** - sorted results from low to high  
**h** - return highest result  
**h4** - return 4 highest results  
**l** - return lowest result  
**l3** - return 3 lowest results  
**ih** - drop highest result  
**ih2** - drop two highest results  
**il** - drop lowest result  
**il4** - drop 4 lowest results  


###Display:

As default results are displayed in form:
  `{query} => {2, 3, 4, 5, 6, 6, 1} = (2, 3, 4, 5, 6, 6, 1)`

You can easy modify it as follows:
If you have in query more than one display directive, only the last one will be used!

**c** : `5d20rc` - counts best results  
`5d6c` - `{...} => {2, 4, 6, 2, 1} = (1)  
`5d6c5` - 5 and 6 are treated as success and counted  

**ic** reverse count  
`3d6ic` - 1's are treated as success  
`3d8ic2` - 1 and 2 are treated as success  

**m** - value of max occurenced dice, shows first result from max occured dices  
**mm** - value of max occurenced dice, shows all results that share the best result  

**im** - value of min occured dice from *results* (there are no'0), one dice  
**imm** - value of min occured dice from *results* (there are no'0), all  

**=** - sum of all dices  
`3d4= => {1,3,3} = (7)`  


##Custom Textual dices (not ready yet)

`1custom{Mary;Jack Lee;Simon}` - 1 result from custom dice (Mary or Jack Lee or Simon).

`2custom{2:Blank;2:focus;3:hit;1:critic}` - 2 results from custom 8sided dice (Xwing attack die)2 sides with Blank result, 2 with Focus, 3 with Hit and the last one with critical hit

With Textual dices you have almost the same filters as with the numeric ones, like explode or branch. But with this kind of dices algorith is not the same. If you define custom textual dice, you define sides from less important to most one. For example, Atack Dice for X-wing miniature game:
`1custom{2:Blank;2:Focus;3:Hit;1:Critic}` - *Blank* is less important (Like '1' on the D6 dice) and *Critical* is most important - like 6 on d6.
 When you need to choose wich side you need you can use name of the side, or number of the side. For example with the dice above. If you want to count hits(and critical hits) you can use:
 `5custom{2:Blank;2:Focus;3:Hit;1:Critic}c[Hit]` or
 `5custom{2:Blank;2:Focus;3:Hit;1:Critic}c3` both examples count all 'Hit' and 'Critic' results.

 What you can do:
 Explodes:
  `r`, `r2` or `r[name]`;
  `rr`, `rr3`, or `rr[name]`;
 explodes low values:
  `ir`, `ir2` or `ir[name]`;
  `irr`, `irr3`, or `irr[name]`;
Branch:
  `b`, `b5` or `b[side]`;
  `ib`, `ib5` or `ib[side]`;

 Filters:
  Sum:
  `s`
  Highest and lowest results
Count, count down,
Max occurences, Min occurences

    In fact you can use anything that isn't directly for numbers, like sum or mathematial operations.


###Multiple types of dices (not redy yet)
You can roll few dices and sum them together
`d6 + 2d8 + 1d100` - 3 results, first 1-6, next 2 from range 1-8 and last from range 1-100

##Querying flow  (not ready yet)
`7k6ir3c3 => Xk6c4`
 You can execute second query after first one. Result of the first query is putted into next query in place 'X' sign.

##Unique dices - dices with unique results (not ready yet)


##Aliases

*D* can be replaced by *K*
*custom* can be replaced by *cu*

##Examples
Eldritch Horror Doubleshot dice - Rolls 5d6 every 5 is treated as success, every 6 is treated as two successes, count succeses

show results:
{5d6b6s} => {1, 2, 4, 5, 6} = (1, 2, 4, 5, 6, 6)
count succeses:
{5d6b6sc5} => {1, 2, 4, 5, 6} = (3)
